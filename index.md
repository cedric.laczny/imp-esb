---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: default
title: Integrated Multi-Omics Pipeline
order: 1
---

Welcome to the website of the Integrated Multi-Omics Pipeline, a.k.a., IMP!

IMP is a reproducible and modular pipeline for large-scale standardized integrated analysis of coupled metagenomic and metatranscriptomic data.
IMP incorporates robust read preprocessing, iterative co-assembly of metagenomic *and* metatranscriptomic data, analyses of microbial community structure and function as well as genomic signature-based visualizations.
As an added functionality, IMP also performs either single-omic metagenomic or metatranscriptomic analyses. 

# IMP version 1
IMP v.1 uses the [Docker](https://www.docker.com/) framework to create a container thereby enabling reproducible installation and execution.
We consider this to be useful in case you have access to your own compute resources, i.e., a server where you have administration rights.

For more information on IMP v.1 please check the [link](http://r3lab.uni.lu/web/imp).

# IMP version 2
While the Docker framework provides several benefits, it requires administration rights.
This, however, is not always possible, in particular in the case of HPC environments.
Hence, we are currently working on porting the IMP installation to use the [conda](https://conda.io/docs/) framework.

In addition, we are working on the implementation of additional functionality to further ease the integration of multi-omic data for microbiome research.

# Cite
> IMP: a pipeline for reproducible reference-independent integrated metagenomic and metatranscriptomic analyses. Genome Biol. 2016 Dec 16;17(1):260.
> Narayanasamy S, Jarosz Y, Muller EE, Heintz-Buschart A, Herold M, Kaysen A, Laczny CC, Pinel N, May P, Wilmes P.

Please cite also the papers of the tools included in IMP!

[Download the PDF from the journal](https://genomebiology.biomedcentral.com/track/pdf/10.1186/s13059-016-1116-8)

[Journal link](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-1116-8)

[Pubmed link](https://www.ncbi.nlm.nih.gov/pubmed/27986083)


