---
layout: page
title: IMP 2.0 Installation Guide
permalink: /IMP 2.0-Installation/
order: 2
---

# Integrated Meta-Omics Pipeline 2.0 (IMP 2.0)
 
The Integrated Meta-omic Pipeline (IMP) is developed to perform large-scale, reproducible and automated integrative reference free analysis of metagenomic and metatranscriptomic data. IMP also performs single omic (i.e. metagenomic-only and metatrancriptomic-only) analysis as an additional functionality.

# Overview of IMP steps : 

IMP is a pipeline that is composed of multiples major steps:

* Preprocessing

* Assembly

* Analysis

* Binning

* Report

To get more insight about how the pipeline is working, please read the paper at [bioRxiv](http://biorxiv.org/content/early/2016/02/10/039263).

### IMP source code

To get the latest version of IMP that uses conda for installation, git clone the IMP repository bioconda branch:

~~~
git clone -b bioconda https://git-r3lab.uni.lu/IMP/IMP.git
~~~ 

## INSTALLATION

#### Basic Miniconda installation:


# *** Install Miniconda3 for 64-bit macOS ***
 To start, download [miniconda3](https://repo.continuum.io/miniconda/Miniconda3-latest-MacOS-x86_64.sh) and install it:
 ``` bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-MacOS-x86_64.sh
bash Miniconda3-latest-MacOS-x86_64.sh
 ```

# *** Install Miniconda3 for 32-bit macOS ***
 To start, download [miniconda3]( https://repo.continuum.io/miniconda/Miniconda3-latest-MacOS-x86_32.sh) and install it:
 ``` bash
wget  https://repo.continuum.io/miniconda/Miniconda3-latest-MacOS-x86_32.sh
bash Miniconda3-latest-MacOS-x86_64.sh
 ```
# *** Install Miniconda3 for 64-bit Linux ***
 To start, download [miniconda3](https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh) and install it:
 ``` bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-MacOS-x86_64.sh
 ```

# *** Install Miniconda3 for 32-bit Linux ***
 To start, download [miniconda3](https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_32.sh) and install it:
 ``` bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_32.sh
bash Miniconda3-latest-MacOS-x86_64.sh
 ```

# Add channels to the conda environment (This step is not required in this case because these commands are added to yaml file)


 ``` bash
 # Please remember that the CHANNEL ORDER IS IMPORTANT
 conda config --add channels defaults
 conda config --add channels conda-forge
 conda config --add channels bioconda
 conda config --add channels imp
 ```

# Creating conda environment from a file that contains list of software packages and dependencies required for IMP

Note: Replace "envname" with the environment name you want to create

~~~
conda env create -n envname -f IMP/conda/envs/all.yaml
~~~

# To see all your environments, run:
~~~
conda info --envs
~~~

Note: The active environment will be marked with an asterisk.

# Activate the environment

~~~
conda activate envname
~~~

Note: Replace "envname" with the environment name used in previous step.

Note: All the Python>3 softwares and dependencies required to run IMP are installed inside the environment. Python 2 dependencies are installed during the first run of the pipeline.

# To see the list of installed packages in the active environment, run:
conda list


# Manual software installation

Before following the steps below, MAKE SURE that the conda environment is activated and the manual installation of the below mentioned softwares (i.e. vcftools and R packages) is done inside the environment.

## vcftools

~~~
wget --no-check-certificate https://webdav-r3lab.uni.lu/public/R3lab/IMP/vcftools_0.1.12b.tar.gz -nv
tar -xzf vcftools_0.1.12b.tar.gz 
cd vcftools_0.1.12b
make
make install 
~~~

Note: copy the binaries and perl files to the bin and lib folder of created environment respectively i.e. "test" here. 

For example:

~~~
cp -r bin/* /miniconda3/envs/test/bin/
cp -r perl/* /miniconda3/envs/test/lib/perl5/site_perl/5.22.0
~~~

# R packages manual installation:
# R with checkpoint libraries
~~~
"library(checkpoint);checkpoint('2016-06-20')"
source('http://bioconductor.org/biocLite.R');biocLite('genomeIntervals', dependencies=TRUE)
~~~

Note: R=3.3.2 version is installed automatically while creating the environment using all.yaml file in previous step. This particular step is required to only install R packages.


# Running IMP

* **Default run to the end of the workflow**

~~~
MG= <input/"MG.R1.fq"> <input/"MG.R2.fq"> MT=<input/"MT.R1.fq"> <input/"MT.R2.fq"> -o <output directory> --use-conda
~~~

`<output directory>` is the directory where output files will be located.

`<input>` are the metagenomics and metatranscriptomics paired input *fastq* files.


# To deactivate the environment, run:
"source deactivate" or "source deactivate envname"

# Issues and request!
Please post them [here](https://git-r3lab.uni.lu/IMP/IMP/issues).

Thank you!

