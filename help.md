---
layout: page
title: Help
permalink: /the-help/
order: 3
---

We are constantly working on improving and extending the functionality of IMP.

We highly welcome your feedback on issues and request!
Please post them [here](https://git-r3lab.uni.lu/IMP/IMP/issues).

Thank you!
