---
# To enable the pagination, do the following changes in this file:
# published: true
# pagination:
#   enabled: true

# And inside index.md, add `published: false`

# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults

title: index 
layout: paginated_index
published: false
pagination:
  enabled: false
---

